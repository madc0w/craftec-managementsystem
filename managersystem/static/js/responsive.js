/*
 * responsible.js: functions for ajax and modals.
 * by Adriano 'madc0ww' Mourao [ github.com/mad0ww ]
 *
 */
function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(
                    cookie.substring(name.length + 1));
                    break;
            }
        }
        return cookieValue;
    }
}

    var csrftoken = getCookie('csrftoken');

    function csrfSafeMethod(method) {
        // these HTTP methods do not require CSRF protection
        return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
    }

    $.ajaxSetup({
        beforeSend: function(xhr, settings) {
            if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
                xhr.setRequestHeader("X-CSRFToken", csrftoken);
            }
        }
    });

// Ticket

var changeActions = {
    "trDone": {
        action: "change_done",
        newelem: '<a href="#" id="trUndo" class="text-success">\
            Tarefa Feita</a>'
    },
    "trUndo": {
        action: "change_done",
        newelem: '<a href="#" id="trDone">Marcar como feito</a>'
    },
    "trOpen": {
        action: "change_closed",
        newelem: '<a href="#" id="trClosed" class="text-danger">\
            Fechado</a>'
    },
   "trClosed": {
        action: "change_closed",
        newelem: '<a href="#" id="trOpen" class="text-success">\
            Aberto</a>'
    },
    "trDoing": {
        action: "change_doing",
        newelem: '<a href="#" id="trBacklog" class="text-success">\
            Backlog</a>'
    },
    "trBacklog": {
        action: "change_doing",
        newelem: '<a href="#" id="trDoing" class="text-warning">\
            Em andamento</a>'
    }
};

function changeTicketStatus(element, action, address, ticket_id, newElement) {
    $.ajax({
        url: address,
        data: {
            "ticket_id": ticket_id,
            "action": action
        },
        datatype: 'json',
        success: function(data) {
            if(data.status_changed && data.element == element) {
                $(element).replaceWith($( newElement ));
                $("#ticketLog").html("<p>" + data.log + "</p>");
            }
        }
    });
}

function removeComment(elem, address) {
    comm = (elem.match("close-(.*)"))[1];
    comm_id = "#comm-" + comm;

    $.ajax({
        url: address,
        method: "POST",
        data: {
            "comm_pk": comm
        },
        datatype: 'json',
        success: function(data) {
            if(data.deleted) {
                $(comm_id).remove();
            }
        }
    });
}

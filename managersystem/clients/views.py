from django.http import HttpResponse
from django.contrib.auth.decorators import login_required

@login_required
def index(request):
    pass

@login_required
def budget(request):
    pass

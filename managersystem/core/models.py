from django.db import models
from django.contrib.auth.models import AbstractUser

"""
    Contact Table: Abstract class representing personal info about an User or a
    Client.
"""
class Contact(models.Model):
    street = models.CharField(max_length=100, default="Rua .")
    neighbourhood = models.CharField(max_length=100, default="Calafate")
    number = models.IntegerField(default="80")
    postal_number = models.CharField(max_length=9, default="303000")
    phone = models.CharField(max_length=10, default="9999999")
    alt_phone = models.CharField(max_length=10, default="99999999")

    class Meta:
        abstract = True

"""
    User Table: Saves data about users who can perform actions on the manager
    system. There are three types of user: Admin, Manager and Common.
"""
class User(AbstractUser, Contact):
    add_date = models.DateTimeField('date added', auto_now_add=True)
    active = models.BooleanField(default=True)

    class Meta:
        permissions = (
                ('ad', 'admin'),
                ('ma', 'manager'),
                ('co', 'common'),
        )

    def __str__(self):
        return self.get_username()

"""
    Stock Product Table: Registers data about the products physically available
    at the companie.
"""
class StockProduct(models.Model):
    name = models.CharField(max_length=50)
    quantitie = models.DecimalField(max_digits=10, decimal_places=2)

"""
    Activity Table: Base table which registers actions of creating new
    registers performed by any user.
"""
class Activity(models.Model):
    add_date = models.DateTimeField('date added', auto_now_add=True)
    added_by = models.ForeignKey(User, on_delete=models.PROTECT)

"""
    Client Table: Keeps data about a single client of the companhie which is
    registered as an action performed by an user.
"""
class Client(Activity, Contact):
    name = models.CharField(max_length=100)
    companie = models.CharField(max_length=200)
    email = models.CharField(max_length=100)

    def __str__(self):
        return self.name

"""
    Ticket Table: Registers a task assigned to an User. Registering it is
    modeled as an action performed by an User.
"""
class Ticket(Activity):
    title = models.CharField(max_length=200)
    description = models.TextField()
    deadline = models.DateField('date added', auto_now_add=False)
    done = models.BooleanField(default=False)
    closed = models.BooleanField(default=False)
    notified = models.BooleanField(default=False)
    doing = models.BooleanField(default=False)
    assignee = models.ForeignKey(User, on_delete=models.PROTECT)
    related_client = models.ForeignKey(Client, null=True, blank=True, on_delete=models.PROTECT)

    @property
    def get_cname(self):
        cname = "Ticket"
        return cname

    def __str__(self):
        return self.title

"""
    Comment Table: An abstract class for generic comments.
"""
class Comment(Activity):
    text = models.TextField()
    deleted = models.BooleanField(default=False)

    class Meta:
        abstract = True

"""
    Ticket Comment Table: Concrete class for ticket's comments.
"""
class TicketComment(Comment):
    related_ticket = models.ForeignKey(Ticket, on_delete=models.PROTECT)

    def __str__(self):
        return self.text

"""
    Request Table: Registers a generical request from a client. It can be a Budget
    Request or an Order Request.
"""
class Request(Ticket):
    BUDGET = 'BG'
    ORDER = 'OD'
    REQ_TYPE = (
            (BUDGET, 'Pedido de orçamento'),
            (ORDER, 'Pedido de entrega'),
    )
    req_type = models.CharField(max_length=2, choices=REQ_TYPE, default=BUDGET)

    @property
    def get_cname(self):
        cname = "Requisição"
        return cname

    def __str__(self):
        return self.title

"""
    Cash Flow Table: Keeps data about an income or an expense. Registering is
    an action performed by an user.
"""
class CashFlow(Activity):
    INCOME = "IN"
    EXPENSE = "EX"
    FLOW = (
            (INCOME, 'Entrada de dinheiro'),
            (EXPENSE, 'Gasto de dinheiro'),
    )
    flow = models.CharField(max_length=2, choices=FLOW, default=INCOME)
    value = models.DecimalField(max_digits=8, decimal_places=2)
    title = models.CharField(max_length=50)
    description = models.TextField()

    def __str__(self):
        return self.title

"""
    Budget Table: Registers a budget required by a client. Registering it is
    modeled as an action performed by an user. A Request can contain one or more
    Budgets.
"""
class Budget(Activity):
    PAGSEGURO = 'PS'
    TRANSACTION = 'TS'
    CASH = 'CS'
    PAY_OPT = (
            (PAGSEGURO, 'PAGSEGURO'),
            (TRANSACTION, 'Transferencia'),
            (CASH, 'Dinheiro'),
    )
    pay_opt = models.CharField(max_length=2, choices=PAY_OPT, default=PAGSEGURO)
    value = models.DecimalField(max_digits=8, decimal_places=2)
    req_days = models.IntegerField()
    req = models.ForeignKey(Request, on_delete=models.PROTECT)

"""
    Item Table: An Item is a component of the product required by the client,
    which is included on a budget. A Budget can contain 1 or more items.
"""
class Item(models.Model):
    item_name = models.CharField(max_length=30)
    quantitie = models.IntegerField()
    unit_value = models.DecimalField(max_digits=6, decimal_places=2)
    value = models.DecimalField(max_digits=6, decimal_places=2)
    budget = models.ForeignKey(Budget, on_delete=models.DO_NOTHING)

"""
    Order Table: Registers an Order required by a client. It is related to one
    budget and a Request can contain one or more Orders. An Order is registered
    as an action performed by an user.
"""
class Order(Activity):
    order_budget = models.OneToOneField(Budget, on_delete=models.PROTECT)
    final_value = models.DecimalField(max_digits=8, decimal_places=2)
    paid = models.BooleanField(default=False)
    canceled = models.BooleanField(default=False)
    order_requested_by = models.ForeignKey(Request, on_delete=models.PROTECT)

"""
    Expenditure Table: Registers a product Expenditure of a single product. It
    is registered as an action performed by an user.
"""
class Expenditure(Activity):
    product = models.OneToOneField(StockProduct, on_delete=models.PROTECT)
    quantitie = models.DecimalField(max_digits=8, decimal_places=2)

"""
    Log Table: Registers all actions related to alter the content of any
    action's registers performed by any user.
"""
class Log(models.Model):
    modified_at = models.DateTimeField('date modified', auto_now_add=True)
    modified_by = models.ForeignKey(User, on_delete=models.PROTECT)

    """
        Polimorphism to a class derivated from Activity
    """
    activity = models.ForeignKey(
            Activity,
            on_delete=models.PROTECT,
            related_name="%(app_label)s_%(class)s_updated_by",
    )

    def __str__(self):
        return "Edited by " + str(self.modified_by) + " at " + \
                str(self.modified_at.strftime('%d/%m/%y, %H:%M'))

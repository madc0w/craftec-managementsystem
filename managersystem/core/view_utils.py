from .models import Ticket

"""
    session_context: function for getting the session objects and return them
    into a dictionary named context.
"""
def session_context(request):
    user = request.user

    notifications = Ticket.objects.filter(notified=False, assignee=user)
    notify_count = notifications.count()

    context = {
            'user': user,
            'notifications': notifications,
            'notify_count': notify_count,
    }

    return context

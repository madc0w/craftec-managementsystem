from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from .models import User, CashFlow, Request, Client, Ticket

admin.site.register(User, UserAdmin)
admin.site.register(CashFlow)
admin.site.register(Request)
admin.site.register(Client)
admin.site.register(Ticket)

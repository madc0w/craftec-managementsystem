# Generated by Django 2.0.1 on 2018-02-07 13:34

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0007_ticket_doing'),
    ]

    operations = [
        migrations.AddField(
            model_name='ticket',
            name='closed',
            field=models.BooleanField(default=False),
        ),
    ]

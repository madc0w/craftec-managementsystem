from django.http import HttpResponse
from django.template import loader
from django.contrib.auth.decorators import login_required
from django.db.models import Sum
from core.view_utils import session_context
from core.models import CashFlow, Request, Order

@login_required
def index(request):
    template = loader.get_template('dashboard/dashboard.html')

    if CashFlow.objects.exists():
        revenue = int(CashFlow.objects.aggregate(Sum('value'))['value__sum'])
    else:
        revenue = 0

    budgets_count = Request.objects.filter(req_type=Request.BUDGET).count()
    requirements = Request.objects.filter(done=False).count()
    orders_act = Order.objects.filter(canceled=False)
    orders_npaid = orders_act.filter(paid=False).count()
    context = session_context(request)

    context['revenue'] = revenue
    context['requirements'] = requirements
    context['budgets_count'] = budgets_count
    context['orders_npaid'] = orders_npaid
    context['dashboard_page'] = True

    return HttpResponse(template.render(context, request))

@login_required
def notifications(request):
    template = loader.get_template('dashboard/notifications.html')
    context = session_context(request)
    context['notifications_page'] = True

    return HttpResponse(template.render(context, request))

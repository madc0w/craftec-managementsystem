"""managersystem URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import include, path
from django.contrib import admin
from django.contrib.auth import views as auth_views
from django.apps import apps

dashboard_name = apps.get_app_config('dashboard').verbose_name
stock_name = apps.get_app_config('stock').verbose_name
cashflow_name = apps.get_app_config('cashflow').verbose_name
clients_name = apps.get_app_config('clients').verbose_name
tickets_name = apps.get_app_config('tickets').verbose_name

urlpatterns = [
    path('login/', auth_views.login, name='login'),
    path('logout/', auth_views.logout, {'next_page': '/login'}, name='logout'),
    path('dashboard/', include(('dashboard.urls', dashboard_name), namespace='dashboard')),
    path('stock/', include(('stock.urls', stock_name), namespace='stock')),
    path('cashflow/', include(('cashflow.urls', cashflow_name), namespace='cashflow')),
    path('clients/', include(('clients.urls', clients_name), namespace='clients')),
    path('tickets/', include(('tickets.urls', tickets_name), namespace='tickets')),
    path('admin/', admin.site.urls),
]

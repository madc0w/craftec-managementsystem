from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('newticket/', views.edit_ticket, name='edit_ticket'),
    path('newticket/saveticket/', views.save_ticket, name='save_ticket'),
    path('editticket/<int:ticket_id>/saveticket/', views.save_ticket,
        name='save_ticket'),
    path('editticket/<int:ticket_id>/', views.edit_ticket, name='edit_ticket'),
    path('ticket/<int:ticket_id>/', views.ticket, name='ticket'),
    path('ticket/<int:ticket_id>/ticketajax/', views.ticket_ajax,
        name='ticket_ajax'),
    path('ticket/<int:ticket_id>/savecomment/', views.save_comment,
        name='save_comment'),
    path('ticket/<int:ticket_id>/deletecomment/', views.delete_comment,
        name='delete_comment'),
]

from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.template import loader
from django.contrib.auth.decorators import login_required
from django.shortcuts import get_object_or_404, get_list_or_404
from core.view_utils import session_context
from core.models import Ticket, User, Client, Request, Log, TicketComment
from django.forms import formset_factory
from .forms import TicketForm, TicketCommentForm
import datetime

@login_required
def index(request):
    template = loader.get_template('tickets/index.html')

    not_tickets = Request.objects.filter(done=False)
    open_tickets = Ticket.objects.filter(
            assignee=request.user,
            doing=False,
            done=False,
            closed=False,
    ).exclude(id__in=not_tickets)
    doing_tickets = Ticket.objects.filter(
            assignee=request.user,
            doing=True,
            done=False,
            closed=False,
    ).exclude(id__in=not_tickets)

    context = session_context(request)

    context['open_tickets'] = open_tickets
    context['doing_tickets'] = doing_tickets
    context['tickets_page'] = True

    return HttpResponse(template.render(context, request))

@login_required
def edit_ticket(request, ticket_id=0):
    template = loader.get_template('tickets/newticket.html')

    context = session_context(request)
    is_new_ticket = True

    if ticket_id != 0:
        is_new_ticket = False
        ticket = get_object_or_404(Ticket, pk=ticket_id)
        initial = {
                'title': ticket.title,
                'assignee': ticket.assignee.pk,
                'deadline': ticket.deadline,
                'description': ticket.description,
        }
        context['form'] = TicketForm(initial=initial)
        context['ticket_name'] = 'T' + str(ticket.pk)
    else:
        context['form'] = TicketForm

    context['tickets_page'] = True
    context['is_new_ticket'] = is_new_ticket

    return HttpResponse(template.render(context, request))

@login_required
def save_ticket(request, ticket_id=0):
    if request.method == 'POST':
        user = get_object_or_404(User, username=request.user)

        form_assignee = get_object_or_404(
                User,
                pk=request.POST.get('assignee'),
        )

        if request.POST.get('related_client'):
            form_related_client = Client.objects.get(
                    pk=request.POST.get('related_client'),
            )
        else:
            form_related = None

        form_description = request.POST.get('description')
        form_title = request.POST.get('title')

        if ticket_id != 0:
            form_deadline = request.POST.get('deadline')

            ticket = Ticket.objects.get(pk=ticket_id)
            ticket.assignee = form_assignee
            ticket.related_client = form_related
            ticket.deadline = form_deadline
            ticket.description = form_description
            ticket.title = form_title

            """
                Log the ticket edition
            """
            edition_log = Log(
                    modified_by=user,
                    activity=ticket,
            )
            edition_log.save()

        elif not Ticket.objects.filter(title=request.POST.get('title')):
            form_deadline = datetime.datetime.strptime(
                    request.POST.get('deadline'),
                    "%m/%d/%Y",
                    ).strftime("%Y-%m-%d")

            ticket = Ticket(
                    description=form_description,
                    title=form_title,
                    deadline=form_deadline,
                    done=False,
                    notified=False,
                    assignee=form_assignee,
                    related_client=form_related,
                    added_by=user,
            )
        ticket.save()
        return HttpResponseRedirect('/tickets/ticket/' + str(ticket.pk) + '/')
    return HttpResponseRedirect('tickets/edit_ticket/' + str(ticket_id) + '/')

@login_required
def save_comment(request, ticket_id):
    if request.method == 'POST':
        commentor = get_object_or_404(User, username=request.user)
        ticket = get_object_or_404(Ticket, pk=ticket_id)
        form_text = request.POST.get('text')

        comment = TicketComment(
                added_by=commentor,
                text=form_text,
                related_ticket=ticket,
        )
        comment.save()

    return HttpResponseRedirect('/tickets/ticket/' + str(ticket_id) + '/')


@login_required
def ticket(request, ticket_id):
    template = loader.get_template('tickets/ticket.html')

    ticket = get_object_or_404(Ticket, pk=ticket_id)
    comments = TicketComment.objects.filter(
            related_ticket_id=ticket_id,
            deleted=False,
    )

    if not ticket.notified:
        ticket.notified = True
        ticket.save()

    context = session_context(request)
    context['ticket'] = ticket
    context['tickets_page'] = True
    context['form'] = TicketCommentForm

    if comments.count() > 0:
        context['comments'] = comments

    try:
        log = Log.objects.filter(activity=ticket).latest('modified_at')
        context['log'] = log
    except:
        pass

    return HttpResponse(template.render(context, request))

@login_required
def ticket_ajax(request, ticket_id):
    ticket = Ticket.objects.get(
            pk=ticket_id,
    )

    data = {}
    action = request.GET.get('action', None)

    if action == 'change_done':
        ticket.done = not ticket.done
        ticket.save()

        element = '#trUndo'
        if ticket.done:
            element = '#trDone'
    elif action == 'change_closed':
        ticket.closed = not ticket.closed
        ticket.save()

        element = '#trClosed'
        if ticket.closed:
            element = '#trOpen'
    elif action == 'change_doing':
        ticket.doing = not ticket.doing
        ticket.save()

        element = '#trDoing'
        if ticket.doing:
            element = '#trBacklog'

    if action == 'change_done' or \
            action == 'change_closed' or \
            action == 'change_doing':
        data['status_changed'] = True
        data['element'] = element

        """
            Log the ticket edition
        """
        edition_log = Log(
                modified_by=request.user,
                activity=ticket,
        )
        edition_log.save()

        data['log'] = edition_log.__str__()


    return JsonResponse(data)

def delete_comment(request, ticket_id):
    data = {}
    if request.method == 'POST':
        comment_id = request.POST.get('comm_pk', None)

        ticket = Ticket.objects.filter(pk=ticket_id)
        comment = TicketComment.objects.get(
                pk=int(comment_id),
        )

        owner = User.objects.get(pk=comment.added_by.pk)

        if request.user == owner:
            comment.deleted = True
            comment.save()

        data['deleted'] = comment.deleted

    return JsonResponse(data)

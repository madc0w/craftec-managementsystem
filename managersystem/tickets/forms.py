from django import forms
from core.models import Ticket, User, Client

class TicketForm(forms.Form):
    title = forms.CharField(
            label='Título',
            max_length=200,
            widget=forms.TextInput(attrs={
                'class': 'form-control border-input',
            }),
    )
    deadline = forms.DateField(
            label='Deadline',
            widget=forms.DateInput(attrs={
                'id': 'datepicker',
                'class': 'form-control border-input',
            }),
    )
    assignee = forms.ModelChoiceField(
            label='Responsável',
            queryset=User.objects.all(),
            empty_label='Selecione o responsável pela tarefa',
            widget=forms.Select(attrs={
                'class': 'form-control border-input',
            }),
    )
    related_client = forms.ModelChoiceField(
            label = 'Cliente',
            queryset=Client.objects.all(),
            empty_label='Selecione o cliente (opcional)',
            required=False,
            widget=forms.Select(attrs={
                'class': 'form-control border-input',
            }),
    )
    description = forms.CharField(
            label='Descrição',
            widget=forms.Textarea(attrs={
                'class': 'form-control border-input',
            }),
    )

    class Meta:
        model = Ticket

class TicketCommentForm(forms.Form):
    text = forms.CharField(
            label='Comentário',
            widget=forms.Textarea(attrs={
                'class': 'form-control border-input',
            }),
    )

    class Meta:
        model = Ticket
